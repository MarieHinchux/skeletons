module Main (..) where

import Color exposing (..)
import Graphics.Collage exposing (..)
import Graphics.Element exposing (..)
import Keyboard
import Window
import Time exposing (..)
import String exposing (toLower)


 -- !!!!! TYPES !!!!!
 -- Game State
type alias GameState =
  {
    started : Bool,
    player : PlayerState
  }

initialGameState =
  {
    started = True
  }

-- Player State
type alias PlayerState =
  {
    x : Float,
    y : Float,
    vx : Float,
    vy : Float,
    dir: Direction
  }

player =
  {
    x = 0,
    y = 0,
    vx = 0,
    vy = 0,
    dir = Right
  }

-- Direction
type Direction = Left | Right

-- Keys
-- see Elm Keybord.arrows.doc
type alias Keys = { x : Int, y : Int }

-- !!!!! PLAYER FUNC !!!!!
run : Keys -> PlayerState -> PlayerState
run keys player =
  { player |
    vx = toFloat keys.x,
    dir =
      if keys.x < 0 then
        Left

      else if keys.x > 0 then
        Right

      else
        player.dir
  }

-- jump : Keys -> PlayerState -> PlayerState
-- jump keys player =
--   if keys.y > 0 && player.vy == 0 then
--       { player | vy = 10.0 }

  -- else
  --     player

-- gravity : Float -> PlayerState -> PlayerState
-- gravity dt player =
--   { player |
--       vy = if player.y > 0 then player.vy - dt/2 else 0
--   }

physics : Float -> PlayerState -> PlayerState
physics dt player =
  { player |
      x = player.x + dt * player.vx,
      y = max 0 (player.y + dt * player.vy)
  }


-- !!!!! UPDATE !!!!!

update : (Float, Keys) -> PlayerState -> PlayerState
update (dt, keys) player =
  player
    |> run keys
    -- |> jump keys
    -- |> gravity dt
    |> physics dt


-- !!!!! VIEW !!!!!

view : (Int, Int) -> PlayerState -> Element
view (w', h') player =
  let
    (w, h) = (toFloat w', toFloat h')

    verb =
      if player.vx /= 0 then
        "Run"

      else if player.vy /= 0 then
        "Jump2"

      else
        "Idle2"

    src = "img/HeroSkeleton/" ++ verb ++ "/Separate/" ++ toLower verb ++ "(1).png"

    playerImage = image 120 260 src

    grassImage = image w' 200 "img/ground.png"

    groundY = 150 - h/2

    position = (player.x, player.y + groundY)
  in
    collage w' h'
      [
        -- Sky
        rect w h
          |> filled (rgb 174 238 238),

        -- grass
        grassImage
          |> toForm
          |> move (0, 140 - h/2),

        -- ground
        rect w 50
          |> filled (rgb 0 0 0)
          |> move (0, 20 - h/2),

        playerImage
          |> toForm
          |> move position
      ]

main : Signal Element
main = Signal.map2 view Window.dimensions (Signal.foldp update player input)

input : Signal (Float, Keys)
input =
  let
    delta = Signal.map (\t -> t/5) (fps 30)
  in
    Signal.sampleOn delta (Signal.map2 (,) delta Keyboard.arrows)
